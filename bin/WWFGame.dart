// ignore_for_file: file_names, non_constant_identifier_names, unrelated_type_equality_checks

import 'dart:io';
import 'Player.dart';
import 'Wordlist.dart';

class WWFGame {
  var Ganename = (r'''

 _    _               _   _    _             ______ _       _     _   
| |  | |             | | | |  | |            |  ___(_)     | |   | |  
| |  | | ___  _ __ __| | | |  | | __ _ _ __  | |_   _  __ _| |__ | |_ 
| |/\| |/ _ \| '__/ _` | | |/\| |/ _` | '__| |  _| | |/ _` | '_ \| __|
\  /\  / (_) | | | (_| | \  /\  / (_| | |    | |   | | (_| | | | | |_ 
 \/  \/ \___/|_|  \__,_|  \/  \/ \__,_|_|    \_|   |_|\__, |_| |_|\__|
                                                       __/ |          
                                                      |___/           
''');

  Player? player;
  int mode = 0;
  int dif = 0;
  int wordletter = 0;
  String? word = " ";

  void Startgame() {
    bool isplay = false;
    while (isplay != true) {
      stdout.write("Select : \n"
          " [1] Play "
          " [2] How to play : ");
      int num = int.parse(stdin.readLineSync()!);

      if (num == 1) {
        isplay = true;
        selectDificulty();
      } else if (num == 2) {
        print(" Word War figth have 3 game mode. \n"
            " Solo, this mode like a hangman game if you can't guess the word you lose . \n"
            " Player vs Player ,this mode you can play with your friend play util someone have no HP and who have HP left is gonna win! \n"
            " Player vs com ,this mode like Player vs Player but if you have no friend you can play this mode ! ");
        isplay = false;
      }
    }
  }

  void selectDificulty() {
    bool isselectDificulty = false;
    while (isselectDificulty != true) {
      stdout.write("Let's "
          "Select dificulty \n"
          " [1] Easy "
          " [2] Normal "
          " [3] Hard : ");
      dif = int.parse(stdin.readLineSync()!);
      if (dif == 1) {
        selectMode();
        isselectDificulty = true;
      } else if (dif == 2) {
        selectMode();
        isselectDificulty = true;
      } else if (dif == 3) {
        selectMode();
        isselectDificulty = true;
      } else {
        isselectDificulty = false;
      }
    }
  }

  void selectMode() {
    bool isSelectMode = false;
    while (isSelectMode != true) {
      stdout.write("Select mode: \n"
          " [1] Solo "
          " [2] Player vs Player : ");
      mode = int.parse(stdin.readLineSync()!);
      if (mode == 1) {
        Play1();
        isSelectMode = true;
      } else if (mode == 2) {
        Play2();
        isSelectMode = true;
      } else {
        isSelectMode = false;
      }
    }
  }

  void Play1() {
    stdout.write("Enter your name : ");
    String name = stdin.readLineSync()!;

    player = Player(name);
    int? count = player?.guessCount;

    word = Wordlist().ramdomword(dif);
    String? answer = word;
    answer = answer?.toLowerCase();
    List word_complete = [];
    int wordletter = answer!.length;
    for (int i = 0; i < wordletter; i++) {
      word_complete.add("_");
    }

    stdout.write(
        "${player!.name} Let's Guess " "the word have $wordletter letter \n");

    //already guess
    List guessedLetters = [];

    while (count! > 0) {
      print("$word_complete \n");
      stdout.write("${player!.name} you can guess $count time! \n");
      stdout.write("What's your guess ? : ");

      String guess = stdin.readLineSync()!;
      guess = guess.toLowerCase();

      if (guess == ("") || guess == (" ")) {
        stdout.write("You didn't guess. Please enter a guess.\n");
      } else if (guessedLetters.contains(guess)) {
        stdout.write("**You already guessed that letter. Guess again.**\n");
      } else if (guess.length > 1) {
        stdout.write(
            "**Your guess is too long, please print only one letter.**\n");
      } else if (!answer.contains(guess)) {
        stdout.write("$guess " "is not in the word! \n");
        count = (count - 1);
        guessedLetters.add(guess);
        print(guessedLetters);
      } else if (answer.contains(guess)) {
        stdout.write("Good job, $guess " "is in the word! \n");
        for (int i = 0; i < wordletter; i++) {
          if (answer[i] == guess) {
            word_complete[i] = guess;
          }
        }
        if (word_complete.join() == answer) {
          stdout.write("You win! The word is $answer \n");
          isWin();
        }
        guessedLetters.add(guess);
        stdout.write("The guessedLetters :$guessedLetters \n");
      }
    }
    islose();
  }

  /*void Play2() {
    stdout.write("Enter your name : ");
    String name1 = stdin.readLineSync()!;
    stdout.write("Enter your name : ");
    String name2 = stdin.readLineSync()!;
    player = Player(name1);
    player = Player(name2);
    int? count = player?.guessCount;
    word = Wordlist().ramdomword(dif);
    String? answer = word;
    answer = answer?.toLowerCase();
    List word_complete = [];
    int wordletter = answer!.length;
    for (int i = 0; i < wordletter; i++) {
      word_complete.add("_");
    }
  }*/

  void islose() {
    stdout.write("You lose! The word is $word \n");
    stdout.write("Do you want to play again? \n"
        " [1] Yes "
        " [2] No : ");
    int num = int.parse(stdin.readLineSync()!);
    if (num == 1) {
      selectMode();
    } else if (num == 2) {
      print("Thank you for playing!");
      Exitgame();
    }
  }

  void isWin() {
    stdout.write("Do you want to play again? \n"
        " [1] Yes "
        " [2] No : ");
    int num = int.parse(stdin.readLineSync()!);
    if (num == 1) {
      Play1();
    } else if (num == 2) {
      print("Thank you for playing!");
      Exitgame();
    }
  }

  void Exitgame() {
    print("See you soon. Peace out!");
    exit(0);
  }
}
